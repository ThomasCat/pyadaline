import numpy as np
import matplotlib.pyplot as plt
import math

LEARNING_RATE = 0.45

def step(x):   # Step function
    if (x > 0): return 1
    else: return -1

def or_linear():    # input dataset representing the logical OR operator (including constant BIAS input of 1)
    INPUTS_OR_LINEAR = np.array([
                    [-1,-1,-1],
                    [-1, 1, 1],
                    [ 1,-1, 1],
                    [ 1, 1, 1]
                ])

    OUTPUTS_OR_LINEAR = np.array([[-1,1,1,1]]).T    # output dataset - Only output a -1 if both inputs are -1    

    np.random.seed(1)    # seed random numbers to make calculation deterministic (just a good practice for testing)
    WEIGHTS = 2*np.random.random((3,1)) - 1    # initialize weights randomly with mean 0
    print ("Random Weights before training", WEIGHTS)
    errors=[]    # Use this list to store the errors
    for iter in range(100):    # Training loop
        for input_item,desired in zip(INPUTS_OR_LINEAR, OUTPUTS_OR_LINEAR):
            ADALINE_OUTPUT = (input_item[0]*WEIGHTS[0]) + (input_item[1]*WEIGHTS[1]) + (input_item[2]*WEIGHTS[2]) # Feed this input forward and calculate the ADALINE output
            ADALINE_OUTPUT = step(ADALINE_OUTPUT) # Run ADALINE_OUTPUT through the step function
            ERROR = desired - ADALINE_OUTPUT # Calculate the ERROR generated
            errors.append(ERROR) # Store the ERROR
            WEIGHTS[0] = WEIGHTS[0] + LEARNING_RATE * ERROR * input_item[0] # Update the weights based on the delta rule
            WEIGHTS[1] = WEIGHTS[1] + LEARNING_RATE * ERROR * input_item[1]
            WEIGHTS[2] = WEIGHTS[2] + LEARNING_RATE * ERROR * input_item[2]

    print ("New Weights after training", WEIGHTS)
    for input_item,desired in zip(INPUTS_OR_LINEAR, OUTPUTS_OR_LINEAR):
        ADALINE_OUTPUT = (input_item[0]*WEIGHTS[0]) + (input_item[1]*WEIGHTS[1]) + (input_item[2]*WEIGHTS[2]) # Feed this input forward and calculate the ADALINE output
        ADALINE_OUTPUT = step(ADALINE_OUTPUT) # Run ADALINE_OUTPUT through the step function
        print ("Actual ", ADALINE_OUTPUT, "Desired ", desired)

    ax = plt.subplot(111) # Plot the errors to see how we did during training
    ax.plot(errors, c='#aaaaff', label='Training Errors')
    ax.set_xscale("log")
    plt.title("ADALINE Errors (2,-2)")
    plt.legend()
    plt.xlabel('Error')
    plt.ylabel('Value')
    plt.show()

def xor_nonlinear():    # input dataset representing the logical OR operator (including constant BIAS input of 1)
    INPUTS_XOR_NONLINEAR = np.array([
                        [-1,-1,-1],
                        [-1, 1, 1],
                        [ 1,-1, 1],
                        [ 1, 1,-1] 
                    ])
    
    OUTPUTS_XOR_NONLINEAR = np.array([[-1,1,1,-1]]).T # output dataset - Only output a -1 if both inputs are -1
   
    np.random.seed(1) # seed random numbers to make calculation deterministic (just a good practice for testing)
    WEIGHTS = 2*np.random.random((3,1)) - 1 # initialize weights randomly with mean 0
    print ("Random Weights before training", WEIGHTS) 
    errors=[] # Use this list to store the errors
    for iter in range(100):    # Training loop
        for input_item,desired in zip(INPUTS_XOR_NONLINEAR, OUTPUTS_XOR_NONLINEAR):
            ADALINE_OUTPUT = (input_item[0]*WEIGHTS[0]) + (input_item[1]*WEIGHTS[1]) + (input_item[2]*WEIGHTS[2]) # Feed this input forward and calculate the ADALINE output
            ADALINE_OUTPUT = step(ADALINE_OUTPUT) # Run ADALINE_OUTPUT through the step function
            ERROR = desired - ADALINE_OUTPUT # Calculate the ERROR generated
            errors.append(ERROR) # Store the ERROR
            WEIGHTS[0] = WEIGHTS[0] + LEARNING_RATE * ERROR * input_item[0] # Update the weights based on the delta rule
            WEIGHTS[1] = WEIGHTS[1] + LEARNING_RATE * ERROR * input_item[1]
            WEIGHTS[2] = WEIGHTS[2] + LEARNING_RATE * ERROR * input_item[2]
    
    print ("New Weights after training", WEIGHTS)
    for input_item,desired in zip(INPUTS_XOR_NONLINEAR, OUTPUTS_XOR_NONLINEAR):
        ADALINE_OUTPUT = (input_item[0]*WEIGHTS[0]) + (input_item[1]*WEIGHTS[1]) + (input_item[2]*WEIGHTS[2]) # Feed this input forward and calculate the ADALINE output
        ADALINE_OUTPUT = step(ADALINE_OUTPUT) # Run ADALINE_OUTPUT through the step function
        print ("Actual ", ADALINE_OUTPUT, "Desired ", desired)
    
    ax = plt.subplot(111) # Plot the errors to see how we did during training
    ax.plot(errors, c='#aaaaff', label='Training Errors')
    ax.set_xscale("log")
    plt.title("ADALINE Errors (2,-2)")
    plt.legend()
    plt.xlabel('Error')
    plt.ylabel('Value')
    plt.show()

or_linear()
xor_nonlinear()